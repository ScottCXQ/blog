import React from 'react';
import Head from 'next/head';
import { Row, Col, Breadcrumb, Icon, Affix } from "antd";
import Header from '../components/Header';
import Author from "../components/Author";
import Ad from "../components/Ad";
import Footer from "../components/Footer";
import '../static/style/pages/detail.css';
import moment from 'moment';
import ReactMarkdown from 'react-markdown';
import MarkdownNavBar from 'markdown-navbar';
import 'markdown-navbar/dist/navbar.css';

const markdown =
  '# P01:课程介绍和环境搭建\n' +
  '[ **M** ] arkdown + E [ **ditor** ] = **Mditor**  \n' +
  '> Mditor 是一个简洁、易于集成、方便扩展、期望舒服的编写 markdown 的编辑器，仅此而已... \n\n' +
   '**这是加粗的文字**\n\n' +
  '*这是倾斜的文字*`\n\n' +
  '***这是斜体加粗的文字***\n\n' +
  '~~这是加删除线的文字~~ \n\n'+
  '\`console.log(111)\` \n\n'+
  '# p02:来个Hello World 初始Vue3.0\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n'+
  '***\n\n\n' +
  '# p03:Vue3.0基础知识讲解\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n\n'+
  '# p04:Vue3.0基础知识讲解\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n\n'+
  '#5 p05:Vue3.0基础知识讲解\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n\n'+
  '# p06:Vue3.0基础知识讲解\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n\n'+
  '# p07:Vue3.0基础知识讲解\n' +
  '> aaaaaaaaa\n' +
  '>> bbbbbbbbb\n' +
  '>>> cccccccccc\n\n'+
  '``` var a=11; ```';

const Detail = () => {
  const oNow = moment().format('YYYY-MM-DD hh:mm:ss');
  
  return (
    <>
      <Head>
        <title>Blog Details</title>
      </Head>
      <Header />
      <div className='comm_main'>
        <Row type='flex' justify='center'>
          <Col className='comm_left' xs={24} sm={24} md={16} lg={18} xl={14}>
            <div className='breadcrumb'>
              <Breadcrumb>
                <Breadcrumb.Item><a href="/">首页</a></Breadcrumb.Item>
                <Breadcrumb.Item>视频教程</Breadcrumb.Item>
                <Breadcrumb.Item>123</Breadcrumb.Item>
              </Breadcrumb>
            </div>
            <div>
              <p className='blog_detail_title'>React实战视频教程-陈小齐Blog开发</p>
              <div className='blog_icon center'>
                <span><Icon type='calendar' /> {oNow}</span>
                <span><Icon type='folder' /> 视频教程</span>
                <span><Icon type='fire' /> 8888</span>
              </div>
              <div className='blog_detail_content'>
                <ReactMarkdown
                  source={markdown}
                  escapeHtml={false}
                />
              </div>
            </div>
          </Col>
          <Col className='comm_right' xs={0} sm={0} md={7} lg={5} xl={4}>
            <Author />
            <Ad />
            
            <Affix offsetTop={5}>
              <div className='blog_nav comm_box'>
                <p className='nav_title'>文章目录</p>
                <MarkdownNavBar
                  source={markdown}
                  ordered={false}
                />
              </div>
            </Affix>
            
          </Col>
        </Row>
        
        <Footer />
        
      </div>
    </>
  );
};

export default Detail;
