import React from 'react';
import '../static/style/components/footer.css';

const Footer = () => (
  <div className='footer'>
    <div>系统由React+Node+Ant Design驱动</div>
    <div>陈小齐</div>
  </div>
);

export default Footer;