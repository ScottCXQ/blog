import React from 'react';
import { Avatar, Divider } from 'antd';
import '../static/style/components/author.css';

const Author = () => {
  return (
    <div className='author comm_box'>
      <div><Avatar size={100} src='https://i0.hdslb.com/bfs/face/1e92e62fc1e39873d64a997fde081d19fa29c449.jpg@140w_140h_1c_100q.webp' /></div>
      <div className='author_intro'>
        <span>专注于全栈开发</span>
        <Divider>社交账号</Divider>
        <Avatar size={28} icon='github' className='account' />
        <Avatar size={28} icon='weibo' className='account' />
        <Avatar size={28} icon='qq' className='account' />
        <Avatar size={28} icon='wechat' className='account' />
      </div>
    </div>
  );
};

export default Author;